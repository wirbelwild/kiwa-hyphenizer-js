/*!
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */
export { Hyphenation } from "./src/Hyphenation";
