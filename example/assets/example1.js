import "./example1.scss";

import { Hyphenation } from "../../index";

const hyphenatedWords = {
    "Silbentrennung": "Silben|trennung"
};

/**
 * We're setting a pipe as hyphenation character here to easier see, where the Hyphenizer adds its hyphens.
 */
Hyphenation.setHyphenCharacter(`<span class="hyphenation-character">|</span>&shy;`, true);

const hyphenation = new Hyphenation(hyphenatedWords);

hyphenation.addElements([
    ...document.querySelectorAll("h1"),
    ...document.querySelectorAll("p")
]);