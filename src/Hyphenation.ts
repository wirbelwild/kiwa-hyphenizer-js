/*!
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

import { getElementsUnified } from "bitandblack-helpers";

interface StringMap {
    [key: string]: string;
}

/**
 * The Hyphenation class adds hyphenation to given elements.
 */
class Hyphenation
{
    static hyphenCharacter: string = "\u00AD";
    
    private readonly hyphenationWords: StringMap;
    
    private elements: Node[] = [];

    private elementsHandled: Node[] = [];
    
    private static isHTMLAllowed: boolean = false;
    
    /**
     * Constructor.
     *
     * @param hyphenationWords A list of words with their hyphenation.
     */
    constructor(hyphenationWords: StringMap)
    {
        this.hyphenationWords = hyphenationWords;
    }

    /**
     * Sets a custom hyphenation character.
     *
     * @param hyphenCharacter {string}
     * @param isHTMLAllowed {boolean}
     */
    static setHyphenCharacter(hyphenCharacter: string, isHTMLAllowed: boolean = false)
    {
        this.hyphenCharacter = hyphenCharacter;
        this.isHTMLAllowed = isHTMLAllowed;
    }
    
    /**
     * Replaces parts of a string with predefined hyphenation possibilities. 
     * 
     * @param input {string}
     * @return {string}
     */
    #replaceString(input: string): string
    {
        const stringParts = input.match(
            /(\p{Letter}+:\p{Letter}+)|(\p{Letter}+\*\p{Letter}+)|(\p{Letter}+_\p{Letter}+)|\p{Letter}+/gu
        );

        if (null === stringParts) {
            return input;
        }

        stringParts.forEach((word) => {
            if ("undefined" === typeof this.hyphenationWords[word]) {
                return;
            }
            
            let wordModified = this.hyphenationWords[word];
            wordModified = wordModified.replaceAll("|", Hyphenation.hyphenCharacter);

            input = input.replaceAll(word, wordModified);
        });

        return input;
    }
    
    /**
     * Adds a list of elements that should get handled.
     * 
     * @param elements A list of elements that should get handled.
     */
    addElements(elements: Node[])
    {
        Hyphenation.isHTMLAllowed
            ? this.handleNestedDOM(elements)
            : this.handleFlatDOM(elements)
        ;
    }

    /**
     * If a hyphenation character contains HTML, we cannot process the DOM in a simple way.
     * To be able to insert the HTML, we need to change all nodes containing text nodes and elements to contain
     * elements consistently. A span element will wrap every text node.
     * 
     * @param elements
     * @private
     */
    private handleNestedDOM(elements: Node[])
    {
        elements.forEach((element: Node) => {
            if (element.nodeType !== Node.ELEMENT_NODE) {
                return;
            }
            
            element.childNodes.forEach((childElement: Node) => {
                if (childElement.hasChildNodes()) {
                    this.addElements(
                        Array.from(childElement.childNodes)
                    );
                }

                if (!(childElement instanceof Text)) {
                    return;
                }

                if (childElement.nodeType === Node.ELEMENT_NODE) {
                    childElement.replaceWith(childElement.cloneNode(true));
                }

                if (childElement.nodeType === Node.TEXT_NODE) {
                    const spanElement = document.createElement("span");
                    spanElement.textContent = childElement.textContent;
                    childElement.replaceWith(spanElement);
                }
            });

            if (!(element instanceof HTMLElement)) {
                return;
            }

            element
                .querySelectorAll("span")
                .forEach((spanElement: HTMLSpanElement) => {
                    spanElement.innerHTML = this.#replaceString(spanElement.innerHTML);
                })
            ;
        });

        this.elementsHandled = getElementsUnified(this.elementsHandled, elements);
    }

    /**
     * Searches and replaces in all text contents.
     * 
     * @param elements
     * @private
     */
    private handleFlatDOM(elements: Node[])
    {
        elements.forEach((element: Node) => {
            if (element.nodeType === Node.ELEMENT_NODE) {
                this.elements.push(element);
            }
            
            if (element.hasChildNodes()) {
                this.addElements(
                    Array.from(element.childNodes)
                );
            }
        });

        this.elements.forEach((item: Node) => {
            item.childNodes.forEach((child: Node) => {

                if (child.nodeType === Node.ELEMENT_NODE) {
                    return;
                }

                child.textContent = this.#replaceString(child.textContent);
            });
        });

        this.elementsHandled = getElementsUnified(this.elementsHandled, this.elements);
        this.elements = [];
    }

    /**
     * Returns all elements that have been added and handled.
     */
    getElementsHandled(): Node[]
    {
        return this.elementsHandled;
    }
}

export { Hyphenation, StringMap };