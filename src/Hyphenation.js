/*!
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Hyphenation_instances, _a, _Hyphenation_replaceString;
import { getElementsUnified } from "bitandblack-helpers";
/**
 * The Hyphenation class adds hyphenation to given elements.
 */
class Hyphenation {
    /**
     * Constructor.
     *
     * @param hyphenationWords A list of words with their hyphenation.
     */
    constructor(hyphenationWords) {
        _Hyphenation_instances.add(this);
        this.elements = [];
        this.elementsHandled = [];
        this.hyphenationWords = hyphenationWords;
    }
    /**
     * Sets a custom hyphenation character.
     *
     * @param hyphenCharacter {string}
     * @param isHTMLAllowed {boolean}
     */
    static setHyphenCharacter(hyphenCharacter, isHTMLAllowed = false) {
        this.hyphenCharacter = hyphenCharacter;
        this.isHTMLAllowed = isHTMLAllowed;
    }
    /**
     * Adds a list of elements that should get handled.
     *
     * @param elements A list of elements that should get handled.
     */
    addElements(elements) {
        _a.isHTMLAllowed
            ? this.handleNestedDOM(elements)
            : this.handleFlatDOM(elements);
    }
    /**
     * If a hyphenation character contains HTML, we cannot process the DOM in a simple way.
     * To be able to insert the HTML, we need to change all nodes containing text nodes and elements to contain
     * elements consistently. A span element will wrap every text node.
     *
     * @param elements
     * @private
     */
    handleNestedDOM(elements) {
        elements.forEach((element) => {
            if (element.nodeType !== Node.ELEMENT_NODE) {
                return;
            }
            element.childNodes.forEach((childElement) => {
                if (childElement.hasChildNodes()) {
                    this.addElements(Array.from(childElement.childNodes));
                }
                if (!(childElement instanceof Text)) {
                    return;
                }
                if (childElement.nodeType === Node.ELEMENT_NODE) {
                    childElement.replaceWith(childElement.cloneNode(true));
                }
                if (childElement.nodeType === Node.TEXT_NODE) {
                    const spanElement = document.createElement("span");
                    spanElement.textContent = childElement.textContent;
                    childElement.replaceWith(spanElement);
                }
            });
            if (!(element instanceof HTMLElement)) {
                return;
            }
            element
                .querySelectorAll("span")
                .forEach((spanElement) => {
                spanElement.innerHTML = __classPrivateFieldGet(this, _Hyphenation_instances, "m", _Hyphenation_replaceString).call(this, spanElement.innerHTML);
            });
        });
        this.elementsHandled = getElementsUnified(this.elementsHandled, elements);
    }
    /**
     * Searches and replaces in all text contents.
     *
     * @param elements
     * @private
     */
    handleFlatDOM(elements) {
        elements.forEach((element) => {
            if (element.nodeType === Node.ELEMENT_NODE) {
                this.elements.push(element);
            }
            if (element.hasChildNodes()) {
                this.addElements(Array.from(element.childNodes));
            }
        });
        this.elements.forEach((item) => {
            item.childNodes.forEach((child) => {
                if (child.nodeType === Node.ELEMENT_NODE) {
                    return;
                }
                child.textContent = __classPrivateFieldGet(this, _Hyphenation_instances, "m", _Hyphenation_replaceString).call(this, child.textContent);
            });
        });
        this.elementsHandled = getElementsUnified(this.elementsHandled, this.elements);
        this.elements = [];
    }
    /**
     * Returns all elements that have been added and handled.
     */
    getElementsHandled() {
        return this.elementsHandled;
    }
}
_a = Hyphenation, _Hyphenation_instances = new WeakSet(), _Hyphenation_replaceString = function _Hyphenation_replaceString(input) {
    const stringParts = input.match(/(\p{Letter}+:\p{Letter}+)|(\p{Letter}+\*\p{Letter}+)|(\p{Letter}+_\p{Letter}+)|\p{Letter}+/gu);
    if (null === stringParts) {
        return input;
    }
    stringParts.forEach((word) => {
        if ("undefined" === typeof this.hyphenationWords[word]) {
            return;
        }
        let wordModified = this.hyphenationWords[word];
        wordModified = wordModified.replaceAll("|", _a.hyphenCharacter);
        input = input.replaceAll(word, wordModified);
    });
    return input;
};
Hyphenation.hyphenCharacter = "\u00AD";
Hyphenation.isHTMLAllowed = false;
export { Hyphenation };
