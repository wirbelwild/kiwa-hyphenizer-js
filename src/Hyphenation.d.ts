/*!
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */
interface StringMap {
    [key: string]: string;
}
/**
 * The Hyphenation class adds hyphenation to given elements.
 */
declare class Hyphenation {
    #private;
    static hyphenCharacter: string;
    private readonly hyphenationWords;
    private elements;
    private elementsHandled;
    private static isHTMLAllowed;
    /**
     * Constructor.
     *
     * @param hyphenationWords A list of words with their hyphenation.
     */
    constructor(hyphenationWords: StringMap);
    /**
     * Sets a custom hyphenation character.
     *
     * @param hyphenCharacter {string}
     * @param isHTMLAllowed {boolean}
     */
    static setHyphenCharacter(hyphenCharacter: string, isHTMLAllowed?: boolean): void;
    /**
     * Adds a list of elements that should get handled.
     *
     * @param elements A list of elements that should get handled.
     */
    addElements(elements: Node[]): void;
    /**
     * If a hyphenation character contains HTML, we cannot process the DOM in a simple way.
     * To be able to insert the HTML, we need to change all nodes containing text nodes and elements to contain
     * elements consistently. A span element will wrap every text node.
     *
     * @param elements
     * @private
     */
    private handleNestedDOM;
    /**
     * Searches and replaces in all text contents.
     *
     * @param elements
     * @private
     */
    private handleFlatDOM;
    /**
     * Returns all elements that have been added and handled.
     */
    getElementsHandled(): Node[];
}
export { Hyphenation, StringMap };
