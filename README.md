[![npm version](https://badge.fury.io/js/kiwa-hyphenizer.svg)](https://www.npmjs.com/package/kiwa-hyphenizer)
[![NPM License](https://img.shields.io/npm/l/kiwa-hyphenizer)](https://www.npmjs.com/package/kiwa-hyphenizer)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Hyphenizer

Perfect hyphenation for your Website.

## Installation

This library is made for the use with [Node](https://www.npmjs.com/package/kiwa-hyphenizer). Add it to your project by running `$ npm install kiwa-hyphenizer` or add it with another dependency manager by your choice.

## Usage

Set up the hyphenation like that:

```javascript
import { Hyphenation } from "kiwa-hyphenizer";

const hyphenatedWords = {
    "Silbentrennung": "Silben|trennung"    
};

const hyphenation = new Hyphenation(hyphenatedWords);

hyphenation.addElements([
    ...document.querySelectorAll("h1"),
    ...document.querySelectorAll("p")
]);
```

### Fetching hyphenated words

You can fetch hyphenated versions of your words from the [Hyphenizer](https://www.hyphenizer.com) API automatically. __Please note__ that the Hyphenizer API requires you to have a valid API token.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
