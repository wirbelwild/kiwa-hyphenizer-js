/*!
 * Kiwa Hyphenizer.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */
import { Hyphenation } from "../src/Hyphenation";
it("Can add hyphenation", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    const element = document.createElement("p");
    element.textContent = "Text mit Silbentrennung.";
    hyphenation.addElements([element]);
    expect(element.innerHTML)
        .toBe("Text mit Silben#trennung.");
});
it("Can hyphenate multiple words", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    const element = document.createElement("p");
    element.textContent = "Text mit Silbentrennung und Silbentrennung und Silbentrennung.";
    hyphenation.addElements([element]);
    expect(element.innerHTML)
        .toBe("Text mit Silben#trennung und Silben#trennung und Silben#trennung.");
    expect(hyphenation.getElementsHandled().length)
        .toBe(1);
});
it("Can hyphenate nested elements", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    const paragraphElement = document.createElement("p");
    paragraphElement.appendChild(document.createTextNode("Text mit Silbentrennung und "));
    const spanElement = document.createElement("span");
    spanElement.textContent = 'verschachtelten Worten mit Silbentrennung';
    spanElement.id = "Silbentrennung";
    paragraphElement.appendChild(spanElement);
    paragraphElement.appendChild(document.createTextNode(" und noch mehr Silbentrennung."));
    hyphenation.addElements([paragraphElement]);
    expect(paragraphElement.innerHTML)
        .toBe(`Text mit Silben#trennung und <span id="Silbentrennung">verschachtelten Worten mit Silben#trennung</span> und noch mehr Silben#trennung.`);
    expect(hyphenation.getElementsHandled().length)
        .toBe(2);
});
it("Leaves nodes and event listeners untouched", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    const paragraphElement = document.createElement("p");
    paragraphElement.appendChild(document.createTextNode("Text mit Silbentrennung und "));
    let hasBeenClicked = false;
    const spanElement = document.createElement("span");
    spanElement.textContent = 'verschachtelten Worten mit Silbentrennung';
    spanElement.addEventListener("click", () => {
        hasBeenClicked = true;
    });
    paragraphElement.appendChild(spanElement);
    paragraphElement.appendChild(document.createTextNode(" und noch mehr Silbentrennung."));
    hyphenation.addElements([paragraphElement]);
    expect(paragraphElement.innerHTML)
        .toBe("Text mit Silben#trennung und <span>verschachtelten Worten mit Silben#trennung</span> und noch mehr Silben#trennung.");
    expect(hasBeenClicked).toBeFalsy();
    spanElement.dispatchEvent(new Event("click"));
    expect(hasBeenClicked).toBeTruthy();
});
it("Blocks html", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter(`<span class="color: red;">&shy;</span>`);
    const hyphenation = new Hyphenation(wordList);
    const element = document.createElement("p");
    element.textContent = "Text mit Silbentrennung.";
    hyphenation.addElements([element]);
    expect(element.innerHTML)
        .toBe(`Text mit Silben&lt;span class="color: red;"&gt;&amp;shy;&lt;/span&gt;trennung.`);
});
it("Can add html to text", () => {
    const wordList = {
        "Silbentrennung": "Silben|trennung"
    };
    Hyphenation.setHyphenCharacter(`<span class="color: red;">&shy;</span>`, true);
    const hyphenation = new Hyphenation(wordList);
    const element = document.createElement("p");
    element.textContent = "Text mit Silbentrennung.";
    hyphenation.addElements([element]);
    expect(element.innerHTML)
        .toBe(`<span>Text mit Silben<span class="color: red;">­</span>trennung.</span>`);
});
it("Can handle gender characters", () => {
    const wordList = {
        "Mütmacher_innen": "Müt|macher_innen",
        "Mütmacher*innen": "Müt|macher*innen",
        "Mütmacher:innen": "Müt|macher:innen",
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    const element = document.createElement("p");
    element.textContent = "Text für Mütmacher_innen, Mütmacher*innen und Mütmacher:innen.";
    hyphenation.addElements([element]);
    expect(element.innerHTML)
        .toBe("Text für Müt#macher_innen, Müt#macher*innen und Müt#macher:innen.");
});
it("Can handle Event Listeners", () => {
    let eventCounter = 0;
    const event = () => {
        ++eventCounter;
    };
    const div = document.createElement("div");
    div.textContent = "Silbentrennung ";
    const link = document.createElement("a");
    link.textContent = "Click me";
    link.addEventListener("click", event);
    div.appendChild(link);
    link.dispatchEvent(new Event("click"));
    const wordList = {
        "Silbentrennung": "Silben|trennung",
    };
    Hyphenation.setHyphenCharacter('#');
    const hyphenation = new Hyphenation(wordList);
    hyphenation.addElements([div]);
    expect(div.innerHTML)
        .toBe("Silben#trennung <a>Click me</a>");
    link.dispatchEvent(new Event("click"));
    expect(eventCounter).toBe(2);
});
