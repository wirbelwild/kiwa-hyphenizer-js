const Encore = require("@symfony/webpack-encore");

Encore
    .setOutputPath("example/build/")
    .setPublicPath("/")
    .addEntry("example1", "./example/assets/example1.js")
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(false)
    .enableSassLoader()
    .enableTypeScriptLoader()
    .disableSingleRuntimeChunk()
    .enableBuildNotifications()
;

module.exports = Encore.getWebpackConfig();
